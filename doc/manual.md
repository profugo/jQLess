# jQLess v0.6b nightly build 20200130

## An extremely minimalist jQuery implementation

**Implemented functions**

 - JQLess(`<elem>`).addClass(`<className>`)
 >`className`:
 Adds the class *className* to the *elem* DOM element
 
 - JQLess.ajax(`<url>`, `<params>`, `<callback>`, `[json]`, `[method]`)
 > Connects with a remote uri to exchange data<br>
 > `url`: remote uri address<br>
 > `params`: array of pairs key => value to be passed as inline params to the uri<br>
 > `callback`: the method to call oncethe exchance succeeds<br>
 > 'json': boolean, defaults to false, if true, JQLess will assume that the call will return a string in JSON format and parse it to return an object<br>
 > `method`: method used to establish the connection, defaults to GET, accepts POST and GET
 
 - JQLess(`<elem>`).append(`<item>`)
 >  appends `<item>` object to `<element>` DOM object
 
 - JQLess(`<elem>`).attr(`<attribute>`, `[value]`)
 - JQLess(`<elem>`).click(`<clickFunction>`)
 - JQLess(`<elem>`).clone()
 - JQLesss.create(`<element>`)
 - JQLess(`<elem>`).css(`<obj>`)
 - JQLess(`<elem>`).each(`<callback>`)
 - JQLess.get(`<url>`, `<callback>`, `[json]`)
 - JQLess(`<elem>`).getDOM()
 - JQLess(`<elem>`).hide()
 - JQLess(`<elem>`).html(`[value]`)
 - JQLess(`<elem>`).on(`<event>`, `<callback>`)
 - JQLess(`<elem>`).parent()
 - JQLess.post(`<url>`, `<params>`, `<callback>`, `[json]`)
 - JQLess(`<elem>`).prop(`<property>`, `[value]`)
 - JQLess().ready(`<execute>`)
 - JQLess(`<elem>`).removeAttr(`<attribute>`)
 - JQLess(`<elem>`).removeClass(`<className>`)
 - JQLess(`<elem>`).resize(`<callBack>`) 
 - JQLess(`<elem>`).show()
 - JQLess(`<elem>`).siblings()
 - JQLess(`<elem>`).submit(`<data>`)
 - JQLess(`<elem>`).text(`<value>`)
 - JQLess(`<elem>`).toggleClass(`<className>`)
 - JQLess(`<elem>`).val(`[value]`)
 - JQLess(window).version

<hr>

*`<elem>`:* In every case it refers to an object identifier, accepting the following formats:

 - `<elem>` : Html tag name, ie: p, div, input. Returns an html objects array.
 - `#<elem>`: The hash prefix (#) means that the element is an id value. Returns an instance of the html object.
 - `.<elem>`: The period prefix (.) means it's reffering to an objects array with the class named in <elem>. Returns an html objects array.

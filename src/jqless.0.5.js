function _JQLess(data) {}

_JQLess.prototype = {
    
    version: "0.6b_20200320.1",
    /**
     * -------------------------------------------------------------------------
     * @param {Object} data
     * @returns {null}
     */
    setParams : function(data)
    {
        var obj = ndata = null;
        var prefijos = ['#', '.'];
        
        if (typeof data === 'string') {
            ndata = data.substring(1);

            obj = document.querySelectorAll(data);
            if (obj.length === 1 ) {
                    obj = obj[0];
            }
        } else {
            if(typeof data === "object") {
                obj = data;
            }
        }
        
        if (Object.prototype.toString.call(this._objArr) === '[object Array]') {
            this._objArr.push(obj);
            this._nameArr.push(ndata);
        } else {
            this._objArr = [obj];
            this._nameArr = [ndata];
        }
        
        this._obj  = obj;
        this._name = ndata;
    },
    
    /**
     * -------------------------------------------------------------------------
     * @param {Object} data
     * @returns {Object}
     */
    updStack : function(data)
    {
        this._obj  = this._objArr.pop();
        this._name = this._nameArr.pop();

        return data;
    },
    
    /**
     * -------------------------------------------------------------------------
     * @param {function} onArray
     * @param {function} onSingle
     * @returns {null}
     */
    _processCollections : function(onArray, onSingle)
    { 
        if( Object.prototype.toString.call( this._obj ) === '[object NodeList]' ) {
            for (var item = 0; item < this._obj.length; item++) {
                onArray(item);
            }
        } else {
            onSingle();
        }
    },
    
    /**
     * -------------------------------------------------------------------------
     * @param {function} execute
     * @returns {null}
     */
    ready: function(execute)
    {
        window.onload = execute;
    },
    
    /**
     * -------------------------------------------------------------------------
     * @param {string || [string]} value
     * @returns {string} _jQLess._obj.innerHTML
     */
    html : function(value)
    {
        this.updStack();
        if (typeof value === 'undefined') {
            return this._obj.innerHTML;
        }

        var param = this._obj;
        
        this._processCollections(
            function(item) { param[item].innerHTML = value; },
            function () { param.innerHTML = value; }
        );
        
        
    },
    
    /**
     * -------------------------------------------------------------------------
     * @param {string || [string]} value
     * @returns {string} _jQLess._obj.innerText || textContent || ""
     */
    text: function(value)
    {
        this.updStack();
        if (typeof value === 'undefined') {
            return this._obj.textContent || this._obj.innerText || "";
        }

        var param = this._obj;
        
        this._processCollections(
            function(item) {param[item].innerHTML = value;},
            function() {param.innerHTML = value;}
        );        
    },
    
    /**
     * -------------------------------------------------------------------------
     * @param {function} callback
     * @returns {null}
     */
    each: function(callback)
    {
        this.updStack();
        for (var _intEachI = 0, l = this._obj.length; _intEachI < l; _intEachI++) {
            callback(this._obj[_intEachI]); 
        }
    },
	
    /**
     * -------------------------------------------------------------------------
     * @returns {DOM_object}
     */
    getDOM: function()
    {
        return this._objArr[this._objArr.length - 1];
    },   
    
    /**
     * -------------------------------------------------------------------------
     * @param {querySelector} className
     * @returns {null}
     */
     removeClass : function(className)
     {
        this.updStack();
        var param  = this._obj;
        var needle = new RegExp('\\b' + className + '\\b');
        this._processCollections(
            function(item) { param[item].className = param[item].className.replace(needle, ''); },
            function () { param.className = param.className.replace(needle, ''); }
        );
     },
     
     /**
     * -------------------------------------------------------------------------
     * @param {string || [string]} className
     * @returns {null}
     */
     toggleClass : function(className)
     {
        this.updStack();
        var param  = this._obj;
        var needle = new RegExp('\\b' + className + '\\b');
        
        this._processCollections(
            function(item) { 
                var i = param[item].className.indexOf(className);
                var temp = param[item].className.replace(needle, '');
                if ( i === -1 ) {
                    param[item].className += ' ' + className;
                } else {
                    param[item].className = temp;
                }
            },
            function () { 
                var i = param.className.indexOf(className);
                var temp = param.className.replace(needle, '').trim();
                if ( i === -1 ) {
                    param.className += ' ' + className;
                } else {
                    param.className = temp;
                }
            }
        );
     },
     
    /**
     * -------------------------------------------------------------------------
     * @param {string || [string]} classNames
     * @returns {null}
     */
     addClass : function(classNames)
     {
        this.updStack();
        var param  = this._obj;
        
        if ( Array.isArray(classNames) === false) {
            classNames = [classNames];
        }
        
        for(var i = 0; i < classNames.length; i++) {
            var className = classNames[i];
            this._processCollections(
                function(item) { param[item].className = (param[item].className + ' ' + className).trim(); },
                function () { param.className = (param.className + ' ' + className).trim(); }
            );
        }
     },
     
    /**
     * -------------------------------------------------------------------------
     * @returns {null}
     */
    hide : function()
    {
        this.updStack();
        var param = this._obj;

        this._processCollections(
            function(item) { param[item].style.display = "none"; },
            function () { param.style.display = "none"; }
        );
    },
    
    /**
     * -------------------------------------------------------------------------
     * @returns {null}
     */
    show : function()
    {
        this.updStack();
        var param = this._obj;
        this._processCollections(
            function(item) { param[item].style.display = "block"; },
            function () { param.style.display = "block"; }
        );
    },
     
    /**
     * -------------------------------------------------------------------------
     * @param {string} event
     * @param {function} callback
     * @returns {null}
     */
    on: function(event, callback)
    {
        this.updStack();
        var param = this._obj;
        this._processCollections(
            function(item) {param[item].addEventListener(event, callback);},
            function() {param.addEventListener(event, callback);}
        );
    },

    /**
     * -------------------------------------------------------------------------
     * @param {object} obj
     * @returns {null}
     */
     css: function(obj)
     {
         this.updStack();
         var selector = this._name;
         var param = this._obj;
         var keys = [];
         
         for(var k in obj) keys.push(k);

         for (var j = 0; j < keys.length; j++) {
             var key = keys[j];
             this._processCollections(
                function(item) { 
                    try {
                        param[item].style[key] = obj[key];
                    } catch (err) {
                        console.log('ERROR: css (multi) [' + err.message + ']');
                    }
                },  
                function () { 
                    try {
                        param.style[key] = obj[key];
                    } catch (err) {
                        console.log('ERROR: css (single) [' + err.message + '] on ' + this._name);
                    }
                }
            );
         }
     },

    /**
     * -------------------------------------------------------------------------
     * @param {function} callBack
     * @returns {null}
     */
     resize : function(callBack)
     {
         this.updStack();
         window.onresize = callBack;
     },
     
    /**
     * -------------------------------------------------------------------------
     * @param {string || [string]} attribute
     * @param {string} value
     * @returns {null}
     */
     attr : function(attribute, value)
     {
        this.updStack();
        if (typeof value === 'undefined') {
            return this._obj.getAttribute(attribute);
        } else {
            var param = this._obj;
            this._processCollections(
                function(item) { param[item].setAttribute(attribute, value); },
                function () { param.setAttribute(attribute, value); }
            );
        }
     },
    
    /**
     * -------------------------------------------------------------------------
     * @param {string} property
     * @param {variant} value
     * @returns {object} value of the property
     */
     prop: function(property, value)
     {
		 this.updStack();
		 if (typeof value === 'undefined') {
			 return this._obj[property];
		 } else {
            var param = this._obj;
            this._processCollections(
                function(item) { param[item][property] = value; },
                function () { param[property] = value; }
            );
        }
	 },
	 
    /**
     * -------------------------------------------------------------------------
     * @returns {_jQLess._obj.parentElement} instance of self parent
     */
     parent : function()
     {
         this.updStack();
         return JQLess(this._obj.parentElement);
     },
     
    /**
     * -------------------------------------------------------------------------
     * @returns {_jQLess._obj.parentNode.childNodes}
     */
     siblings : function()
     {
         this.updStack();
         return this._obj.parentNode.childNodes;
     },
     
    /**
     * -------------------------------------------------------------------------
     * @param {string} attribute
     * @returns {null}
     */
     removeAttr : function(attribute)
     {
         this.updStack();
         this._obj.removeAttribute(attribute);
     },
     
    /**
     * -------------------------------------------------------------------------
     * @param {var} value
     * @returns {null}
     */
    val : function(value)
    {
        this.updStack();
        if (typeof value === 'undefined') {
            return this._obj.value;
        }
        
        this._obj.value = value;
    },
    
    /**
     * -------------------------------------------------------------------------
     * @param {function} clickFunction
     * @returns {null}
     */
    click : function(clickFunction)
    {
        this.updStack();
        /*
         * If method calledwithout params and the element clicked has an onClick
         * handler then call the handler
         */
        if (typeof clickFunction === 'undefined') {
            if (typeof this._obj.onclick === "function") {
                 try {
                    this._obj.onclick.apply(this._obj);
                    return;
                } catch (err) {
                    console.log('ERROR: click [' + err.message + ']');
                }

            }
        }
		
        var param = this._obj;
        this._processCollections(
            function(item) { 
                try {
                    param[item].onclick = clickFunction; 
                } catch (err) {
                    console.log('ERROR: click (multi) [' + err.message + ']');
                }
                },
            function () { 
                try {
                    param.onclick = clickFunction; 
                } catch (err) {
                    console.log('ERROR: click (single) [' + err.message + '] on (' +this._name + ') ');
                }
            }
        );
    },
    
    /**
     * -------------------------------------------------------------------------
     * @param {funcion} data
     * @returns {null}
     */
    submit : function(data)
    {
        this.updStack();
        if (typeof data === 'function') {
            this._obj.onsubmit = data;
            return;
        }

        if (this._obj.onsubmit) {
            var result = this._obj.onsubmit.call(this._obj);
        }
        
        if (result !== false) {
            this._obj.submit();
        }
        
    },
    
    /**
     * -------------------------------------------------------------------------
     * @param {DOM_object} item
     * @returns {null}
     */
     append : function(item)
     {
         this.updStack();
         this._obj.appendChild(item);
     }
    
};

function JQLess(data)
{
    /**
     * Ensures only one instance of JQLess exists in memory
     * @param {type} data
     * @returns {JQLess.singleton}
     */
    function singleton(data)
    {
        if (typeof this._contenedor === 'undefined') {
            this._contenedor = new _JQLess();
        }
        this._contenedor.setParams(data);
    }
    
    // Procecess the $(function() {}); alias returning an instance of JQLess().ready(data);
    if (typeof data === 'function') {
        this._contenedor = new _JQLess();
        return this._contenedor.ready(data);
    }
       
    singleton(data);
    return this._contenedor;
}

/**
 * -----------------------------------------------------------------------------
 * @param {string} url
 * @param {object} params
 * @param {function} callback
 * @param {boolean} json
 * @param {POST | GET} method
 * @returns {null}
 */
JQLess.ajax = function(url, params, callback, json, method)
    {
        if (method === "undefined") {
            method = "GET";
        }
        
        params = Object.keys(params).map(function(k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(params[k]);
        }).join('&');
        
        var xmlhttp = new XMLHttpRequest();
        
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                ret = xmlhttp.responseText;
                if (json) {
                    ret = JSON.parse(ret);
                }
                callback(ret);
            }
        };
        xmlhttp.open(method, url, true);
        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xmlhttp.send(params);
    };

/**
 * -----------------------------------------------------------------------------
 * @param {string} url
 * @param {object} params
 * @param {function} callback
 * @param {boolean} json
 * @returns {null|undefined}
 */
JQLess.post = function(url, params, callback, json)
{
    JQLess.ajax(url, params, callback, json, "POST");
};

/**
 * -----------------------------------------------------------------------------
 * @param {string} url
 * @param {function} callback
 * @param {boolean} json
 * @returns {null|undefined}
 */
JQLess.get = function(url, callback, json)
{
    JQLess.ajax(url, {}, callback, json, "GET");
};

/**
 * -----------------------------------------------------------------------------
 * @param {string} element
 * @returns {DOM_Element}
 */
JQLess.create = function(element)
{ 
    return document.createElement(element); 
};
    
/**
 * -------------------------------------------------------------------------
 * @param {DOM_object} obj
 * @returns {DOM_object}
 */
JQLess.clone = function(obj)
{
    return JSON.parse(JSON.stringify(obj));
};

var $ = jQuery = JQLess;

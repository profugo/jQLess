'use strict';

class _jQLess {
    constructor() {
        this.version = '0.5.a';
    }

    /**
     * -------------------------------------------------------------------------
     * @param {Object} data
     * @returns {null}
     */
    setParams(data) {
        let obj,
            ndata = null;

        if (typeof data === 'string') {
            ndata = data.substring(1);
            obj = document.querySelectorAll(data);
            if (obj.length === 1) {
                obj = obj[0];
            }
        } else {
            if (typeof data === "object") {
                obj = data;
            }
        }

        if (Object.prototype.toString.call(this._objArr) === '[object Array]') {
            this._objArr.push(obj);
            this._nameArr.push(ndata);
        } else {
            this._objArr = [obj];
            this._nameArr = [ndata];
        }

        this._obj = obj;
        this._name = ndata;
    }

    /**
     * -------------------------------------------------------------------------
     * @param {Object} data
     * @returns {Object}
     */
    updStack(data) {
        this._obj = this._objArr.pop();
        this._name = this._nameArr.pop();

        return data;
    }

    /**
     * -------------------------------------------------------------------------
     * @param {function} onArray
     * @param {function} onSingle
     * @returns {null}
     */
    _processCollections(onArray, onSingle) {
        if (Object.prototype.toString.call(this._obj) === '[object NodeList]') {
            for (let item = 0, l = this._obj.length; item < l; item++) {
                onArray(item);
            }
        } else {
            onSingle();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * @param {function} execute
     * @returns {null}
     */
    ready(execute) {
        window.onload = execute;
    }

    /**
     * -------------------------------------------------------------------------
     * @param {string || [string]} value
     * @returns {string} _jQLess._obj.innerHTML
     */
    html(value) {
        this.updStack();
        if (typeof value === 'undefined') {
            return this._obj.innerHTML;
        }

        let param = this._obj;

        this._processCollections(item => param[item].innerHTML = value, () => param.innerHTML = value);
    }

    /**
     * -------------------------------------------------------------------------
     * @param {string || [string]} value
     * @returns {string} _jQLess._obj.innerText || textContent || ""
     */
    text(value) {
        this.updStack();
        if (typeof value === 'undefined') {
            return this._obj.textContent || this._obj.innerText || "";
        }

        let param = this._obj;

        this._processCollections(item => param[item].innerHTML = value, () => param.innerHTML = value);
    }

    /**
     * -------------------------------------------------------------------------
     * @param {function} callback
     * @returns {null}
     */
    each(callback) {
        this.updStack();
        for (let _intEachI = 0, l = this._obj.length; _intEachI < l; _intEachI++) {
            callback(this._obj[_intEachI]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * @returns {DOM_object}
     */
    getDOM() {
        return this._objArr[this._objArr.length - 1];
    }

    /**
     * -------------------------------------------------------------------------
     * @param {querySelector} className
     * @returns {null}
     */
    removeClass(className) {
        this.updStack();
        let param = this._obj;
        let needle = new RegExp('\\b' + className + '\\b');
        this._processCollections(item => param[item].className = param[item].className.replace(needle, ''), () => param.className = param.className.replace(needle, ''));
    }

    /**
     * -------------------------------------------------------------------------
     * @param {string || [string]} className
     * @returns {null}
     */
    toggleClass(className) {
        this.updStack();
        let param = this._obj;
        let needle = new RegExp('\\b' + className + '\\b');

        this._processCollections(item => {
            let i = param[item].className.indexOf(className);
            let temp = param[item].className.replace(needle, '');
            if (i === -1) {
                param[item].className += ' ' + className;
            } else {
                param[item].className = temp;
            }
        }, () => {
            let i = param.className.indexOf(className);
            let temp = param.className.replace(needle, '').trim();
            if (i === -1) {
                param.className += ' ' + className;
            } else {
                param.className = temp;
            }
        });
    }

    /**
     * -------------------------------------------------------------------------
     * @param {string || [string]} className
     * @returns {null}
     */
    addClass(className) {
        this.updStack();
        let param = this._obj;

        this._processCollections(item => param[item].className = (param[item].className + ' ' + className).trim(), () => param.className = (param.className + ' ' + className).trim());
    }

    /**
     * -------------------------------------------------------------------------
     * @returns {null}
     */
    hide() {
        this.updStack();
        let param = this._obj;

        this._processCollections(item => param[item].style.display = "none", () => param.style.display = "none");
    }

    /**
     * -------------------------------------------------------------------------
     * @returns {null}
     */
    show() {
        this.updStack();
        let param = this._obj;
        this._processCollections(item => param[item].style.display = "block", () => param.style.display = "block");
    }

    /**
     * -------------------------------------------------------------------------
     * @param {string} event
     * @param {function} callback
     * @returns {null}
     */
    on(event, callback) {
        this.updStack();
        let param = this._obj;
        this._processCollections(item => param[item].addEventListener(event, callback), () => param.addEventListener(event, callback));
    }

    /**
     * -------------------------------------------------------------------------
     * @param {object} obj
     * @returns {null}
     */
    css(obj) {
        this.updStack();
        let param = this._obj;
        let keys = [];

        for (let k in obj) keys.push(k);

        for (let j = 0, l = keys.length; j < l; j++) {
            let key = keys[j];
            this._processCollections(item => {
                try {
                    param[item].style[key] = obj[key];
                } catch (err) {
                    console.log('ERROR: css (multi) [' + err.message + ']');
                }
            }, () => {
                try {
                    param.style[key] = obj[key];
                } catch (err) {
                    console.log('ERROR: css (single) [' + err.message + '] on ' + this._name);
                }
            });
        }
    }

    /**
     * -------------------------------------------------------------------------
     * @param {function} callBack
     * @returns {null}
     */
    resize(callBack) {
        this.updStack();
        window.onresize = callBack;
    }

    /**
     * -------------------------------------------------------------------------
     * @param {string || [string]} attribute
     * @param {string} value
     * @returns {null}
     */
    attr(attribute, value) {
        this.updStack();
        if (typeof value === 'undefined') {
            return this._obj.getAttribute(attribute);
        } else {
            let param = this._obj;
            this._processCollections(item => param[item].setAttribute(attribute, value), () => param.setAttribute(attribute, value));
        }
    }

    /**
     * -------------------------------------------------------------------------
     * @returns {_jQLess._obj.parentElement} instance of self parent
     */
    parent() {
        this.updStack();
        return jQLess(this._obj.parentElement);
    }

    /**
     * -------------------------------------------------------------------------
     * @returns {_jQLess._obj.parentNode.childNodes}
     */
    siblings() {
        this.updStack();
        return this._obj.parentNode.childNodes;
    }

    /**
     * -------------------------------------------------------------------------
     * @param {string} attribute
     * @returns {null}
     */
    removeAttr(attribute) {
        this.updStack();
        this._obj.removeAttribute(attribute);
    }

    /**
     * -------------------------------------------------------------------------
     * @param {var} value
     * @returns {null}
     */
    val(value) {
        this.updStack();
        if (typeof value === 'undefined') {
            return this._obj.value;
        }

        this._obj.value = value;
    }

    /**
     * -------------------------------------------------------------------------
     * @param {function} clickFunction
     * @returns {null}
     */
    click(clickFunction) {
        this.updStack();
        // If called without parameters and the target element has an onclick 
        // event hanler then call the handler
        if (typeof clickFunction === 'undefined') {
            if (typeof this._obj.onclick === "function") {
                try {
                    this._obj.onclick.apply(this._obj);
                    return;
                } catch (err) {
                    console.log('ERROR: click [' + err.message + ']');
                }
            }
        }

        let param = this._obj;
        this._processCollections(item => {
            try {
                param[item].onclick = clickFunction;
            } catch (err) {
                console.log('ERROR: click (multi) [' + err.message + ']');
            }
        }, () => {
            try {
                param.onclick = clickFunction;
            } catch (err) {
                console.log('ERROR: click (single) [' + err.message + '] on (' + this._name + ') ');
            }
        });
    }

    /**
     * -------------------------------------------------------------------------
     * @param {funcion} data
     * @returns {null}
     */
    submit(data) {
        this.updStack();
        if (typeof data === 'function') {
            this._obj.onsubmit = data;
            return;
        }

        var result = false;
        if (this._obj.onsubmit) {
            result = this._obj.onsubmit.call(this._obj);
        }

        if (result !== false) {
            this._obj.submit();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * @param {DOM_object} item
     * @returns {null}
     */
    append(item) {
        this.updStack();
        this._obj.appendChild(item);
    }

};

/**
 * -----------------------------------------------------------------------------
 * @param {object} data
 * @returns {_jQLess|null}
 */
var jQLess = function (data) {
    var _contenedor = {};
    // Procesa el alias $(function() {}); que devuelve
    // una instancia de jQLess().ready(data);
    if (typeof data === 'function') {
        _contenedor = new _jQLess();
        return _contenedor.ready(data);
    }

    if (Object.keys(_contenedor).length === 0) {
        _contenedor = new _jQLess();
    }
    _contenedor.setParams(data);
    return _contenedor;
};

/**
 * -----------------------------------------------------------------------------
 * @param {string} url
 * @param {object} params
 * @param {function} callback
 * @param {boolean} json
 * @param {POST | GET} method
 * @returns {null}
 */
jQLess.ajax = function (url, params, callback, json, method) {
    method = method || "GET";

    fetch(url, {
        method: method,
        headers: {
            'Content-type': 'application/json',
            body: JSON.stringify(params)
        }
    }).then(ret => {
        if (json) {
            return ret.json();
        } else {
            return ret.text();
        }
    }).then(ret => callback(ret));
};

/**
 * -----------------------------------------------------------------------------
 * @param {string} url
 * @param {object} params
 * @param {function} callback
 * @param {boolean} json
 * @returns {null|undefined}
 */
jQLess.post = (url, params, callback, json) => jQLess.ajax(url, params, callback, json, "POST");

/**
 * -----------------------------------------------------------------------------
 * @param {string} url
 * @param {function} callback
 * @param {boolean} json
 * @returns {null|undefined}
 */
jQLess.get = (url, callback, json) => jQLess.ajax(url, {}, callback, json, "GET");

/**
 * -----------------------------------------------------------------------------
 * @param {string} element
 * @returns {DOM_Element}
 */
jQLess.create = element => {
    return document.createElement(element);
};

const $ = jQLess;
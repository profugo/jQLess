# jQLess
An extremely minimalist jQuery implementation with a reduced set of instruccions

## Getting Started

I should really write something more or less serious here

### Prerequisites
To grant a correct execution of the class, you need a navigator that supports ECMAScript 3 or above.


### Installing

You only need to include the javascript file in your html ie:
````javascript
<script language="JavaScript" type="text/javascript" src="js/jqless.min.js"></script>
````

A couple examples of usage here

```javascript
var use='your imagination';
```

End with an example of getting some data out of the system or using it for a little demo


## Included methods
See doc/manual.md for a complete list

## Deployment

Add additional notes about how to deploy this on a live system


## License

This project is licensed under the [MIT License](https://mit-license.org/) - follow the link for details

## Acknowledgments

* The [jQuery](https://jquery.com) team for they awesome work
* Inspiration
* etc
